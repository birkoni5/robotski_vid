import LV1
import cv2
import math
import numpy as np


def camera_calibration(board_x, board_y, num_of_pics):
    # termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    board_dim = (board_x, board_y)
    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((board_dim[0]*board_dim[1], 3), np.float32)
    objp[:, :2] = np.mgrid[0:board_dim[0], 0:board_dim[1]].T.reshape(-1, 2)

    # Arrays to store object points and image points from all the images.
    objpoints = []  # 3d point in real world space
    imgpoints = []  # 2d points in image plane.
    successes = 0
    cap = cv2.VideoCapture(0) # , cv2.CAP_DSHOW -- rješenje za [ WARN:0] terminating async callback
    while successes < num_of_pics:
        ret, frame = cap.read()
        img = frame.copy()
        cv2.putText(img, 'Press Space to determine chessboard corners',
                    (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)
        cv2.putText(img, 'or ESC to go back to the main menu.',
                    (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)
        cv2.putText(img, "Images needed: "+str(10-successes), (10, 70),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1)
        cv2.imshow('Current view', img)
        key = cv2.waitKey(1) & 0xFF
        if key == 27:
            return [], []
        elif key == 32:
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            # Find the chess board corners
            ret, corners = cv2.findChessboardCorners(gray, board_dim, None)
            # If found, add object points, image points (after refining them)
            if ret:
                objpoints.append(objp)
                corners2 = cv2.cornerSubPix(
                    gray, corners, (11, 11), (-1, -1), criteria)
                imgpoints.append(corners2)

                # Draw and display the corners
                corners = cv2.drawChessboardCorners(
                    frame, board_dim, corners2, ret)
                cv2.imshow('Calibration', corners)
                cv2.waitKey(500)
                successes += 1
    cap.release()
    cv2.destroyAllWindows()

    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(
        objpoints, imgpoints, gray.shape[::-1], None, None)

    np.savez('cameraparams', mtx, dist)
    return mtx, dist


def load_camera_paremeters():
    npzfile = np.load('cameraparams.npz')
    mtx = npzfile[npzfile.files[0]]
    dist = npzfile[npzfile.files[1]]
    return mtx, dist


def make_undistored(img, mtx, dist):
    h,  w = img.shape[:2]
    newcameramtx, roi = cv2.getOptimalNewCameraMatrix(
        mtx, dist, (w, h), 1, (w, h))

    # undistort
    dst = cv2.undistort(img, mtx, dist, None, newcameramtx)

    # crop the image
    x, y, w, h = roi
    dst = dst[y:y+h, x:x+w]
    return dst


def show_undistored(mtx, dist):
    cap = cv2.VideoCapture(0)
    while(True):
        ret, frame = cap.read()
        cv2.imshow('Original view', frame)
        dst = make_undistored(frame, mtx, dist)
        cv2.imshow('Undistorted view', dst)

        key = cv2.waitKey(1) & 0xFF
        if key == 27:
            break
    cap.release()
    cv2.destroyAllWindows()
    return

def take_undistored_frame(mtx, dist):
    cap = cv2.VideoCapture(0) # , cv2.CAP_DSHOW -- rješenje za [ WARN:0] terminating async callback
    while(True):
        ret, frame = cap.read()
        dst = make_undistored(frame, mtx, dist)
        img = dst.copy()
        cv2.putText(dst, 'Press Space to take undistored frame.',
                    (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)
        cv2.imshow('Undistorted view', dst)
        key = cv2.waitKey(1) & 0xFF
        if key == 27:
            result = []
            break
        elif key == 32:
            dst = img.copy()
            result = img.copy()
            cv2.putText(dst, '[c] - Confirm',
                    (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)
            cv2.putText(dst, '[r] - Reset',
                    (10, 40), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)
            cv2.imshow('Frame', dst)
            key = cv2.waitKey(1) & 0xFF
        elif cv2.getWindowProperty('Frame', 0) != -1 and key == ord('c'):
            break
        elif key == ord('r'):
            cv2.destroyWindow('Frame')
    cap.release()
    cv2.destroyAllWindows()
    return result

refPt, image = [], []


def select_point(event, x, y, flags, param):
    global refPt, image
    if event == cv2.EVENT_LBUTTONDOWN and len(refPt) < 4:
        refPt.append((x, y))
        cv2.circle(image, (x, y), 6, (0, 255, 0), 2)
        cv2.imshow("image", image)
    return


def select4points(img):
    global refPt, image
    image = img.copy()
    cv2.putText(image, 'Select 4 point and confirm selection with pressing c',
                (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 0), 1)
    cv2.putText(image, 'press r to reset selection or press ESC to go back',
                (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 0), 1)
    cv2.putText(image, 'to the main menu. ORDER: Top L - R -> Bottom R - L',
                (10, 70), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 0), 1)
    cv2.namedWindow("image")
    cv2.setMouseCallback("image", select_point)
    while True:
        cv2.imshow("image", image)
        key = cv2.waitKey(1) & 0xFF
        if len(refPt) == 4 and key == ord('c'):
            cv2.destroyAllWindows()
            result = refPt.copy()
            refPt.clear()
            return result
        elif key == ord('r'):
            image = img.copy()
            cv2.putText(image, 'Select 4 point and confirm selection with pressing c',
                        (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 0), 1)
            cv2.putText(image, 'press r to reset selection or press ESC to go back',
                        (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 0), 1)
            cv2.putText(image, 'to the main menu. ORDER: Top L - R -> Bottom R - L',
                        (10, 70), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 0), 1)
            refPt.clear()
        elif key == 27:  # ako je pritisnuta tipka ESC vraća se izvorna slika
            cv2.destroyAllWindows()
            refPt.clear()
            return []

# Reference: https://www.pyimagesearch.com/2014/08/25/4-point-opencv-getperspective-transform-example/


def order_points(pts):
    # initialzie a list of coordinates that will be ordered
    # such that the first entry in the list is the top-left,
    # the second entry is the top-right, the third is the
    # bottom-right, and the fourth is the bottom-left
    rect = np.zeros((4, 2), dtype="float32")

    # the top-left point will have the smallest sum, whereas
    # the bottom-right point will have the largest sum
    pts = np.asarray(pts)
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]

    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]

    # return the ordered coordinates
    return rect


def four_point_transform(image, pts):
        # obtain a consistent order of the points and unpack them
        # individually
    rect = order_points(pts)
    (tl, tr, br, bl) = rect

    # compute the width of the new image, which will be the
    # maximum distance between bottom-right and bottom-left
    # x-coordiates or the top-right and top-left x-coordinates
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))

    # compute the height of the new image, which will be the
    # maximum distance between the top-right and bottom-right
    # y-coordinates or the top-left and bottom-left y-coordinates
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))
    # now that we have the dimensions of the new image, construct
    # the set of destination points to obtain a "birds eye view",
    # (i.e. top-down view) of the image, again specifying points
    # in the top-left, top-right, bottom-right, and bottom-left
    # order
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype="float32")

    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))

    # return the warped image
    return warped


def find_dominant_line(img):
    dst = cv2.Canny(img, 50, 200, None, 3)
    cdst = cv2.cvtColor(dst, cv2.COLOR_GRAY2BGR)
    lines = cv2.HoughLines(dst, 1, np.pi / 180, 200, None, 0, 0)
    try:
        return lines[0][0][:]
    except TypeError:
        return []


def draw_line(img, line):
    rho = line[0]
    theta = line[1]
    a = math.cos(theta)
    b = math.sin(theta)
    x0 = a * rho
    y0 = b * rho
    pt1 = (int(x0 + 1000*(-b)), int(y0 + 1000*(a)))
    pt2 = (int(x0 - 1000*(-b)), int(y0 - 1000*(a)))
    cv2.line(img, pt1, pt2, (0, 255, 0), 2, cv2.LINE_AA)
    return img


def draw_distance(img, line, resolution):
    rho = line[0]
    theta = line[1]
    a = math.cos(theta)
    b = math.sin(theta)
    x = int(a * rho)
    y = int(b * rho)
    x3d = a * rho * resolution[0]
    y3d = b * rho * resolution[1]
    distance = np.sqrt(x3d**2+y3d**2)
    cv2.arrowedLine(img, (0, 0), (x, y), (255, 0, 0), 2, cv2.LINE_AA)
    cv2.arrowedLine(img, (x, y), (0, 0), (255, 0, 0), 2, cv2.LINE_AA)
    cv2.putText(img, 'Distance: %.5fcm'%distance,
                (np.shape(img)[1]-220, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 0), 2)
    return img
"""
import sys
#mtx, dist = camera_calibration(6,8,10)
mtx,dist = load_camera_paremeters()
#show_undistored(mtx, dist)
img = take_undistored_frame(mtx, dist)
if img == []:
    sys.exit(0)
dim = (25.1, 17)
pts = select4points(img)
if len(pts) == 0:
    sys.exit(0)
warped = four_point_transform(img, pts)
cv2.imshow("Warped", warped)
cv2.waitKey(0)
cv2.destroyAllWindows()
res = (dim[0]/np.shape(warped)[1], dim[1]/np.shape(warped)[0])
line = find_dominant_line(warped)
if line == []:
    sys.exit(0)
img = draw_line(warped, line)
img = draw_distance(warped, line, res)
LV1.show_image("Image", img)
"""
