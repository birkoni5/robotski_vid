import numpy as np
import cv2

def Convert2DPointsTo3DPoints(pts2D_L,pts2D_R, E, P):
    P = np.matrix(P)
    D, U, Vt = cv2.SVDecomp(E)

    W = [[0, -1, 0],
         [1, 0, 0],
         [0, 0, 1]]
    W = np.float32(W)

    A = np.matrix(U.dot(W).dot(Vt))

    b = np.zeros([3,1], np.float)
    b[:,0]=U[:,2]

    Ainv_b = A.I.dot(b)

    Lpi, Rpi, ARpi = np.zeros([3,1], np.float),np.zeros([3,1], np.float),np.zeros([3,1], np.float)

    S = np.zeros([2,1], np.float)

    X = np.zeros([2,2], np.float)
    x1, x2, x4 = np.zeros([1,1], np.float), np.zeros([1,1], np.float), np.zeros([1,1], np.float)

    Y = np.zeros([2,1], np.float)
    y1, y2 = np.zeros([1,1], np.float), np.zeros([1,1], np.float)

    Lm, Rm = np.zeros([3,1], np.float),np.zeros([3,1], np.float)

    pts3D = []

    for i in range(len(pts2D_L)):
        Lm[0,0] = pts2D_L[i][0]
        Lm[1,0] = pts2D_L[i][1]
        Lm[2,0] = 1

        Rm[0,0] = pts2D_R[i][0]
        Rm[1,0] = pts2D_R[i][1]
        Rm[2,0] = 1

        Lpi = P.I.dot(Lm)
        Rpi = P.I.dot(Rm)

        #Init X table
        ARpi = A.I.dot(Rpi)
        x1 = Lpi.T.dot(Lpi)
        x2 = Lpi.T.dot(ARpi)
        x4 = ARpi.T.dot(ARpi)

        X[0,0], X[0,1], X[1,0], X[1,1] = -x1[0,0], x2[0,0], x2[0,0], -x4[0,0]
        
        #Init Y table
        y1 = Lpi.T.dot(Ainv_b)
        y2 = ARpi.T.dot(Ainv_b)
        Y[0,0] = -y1[0,0]
        Y[1,0] = y2[0,0]

        cv2.solve(X, Y, S)

        s = S[0,0]
        t = S[1,0]

        Lpi = s*Lpi
        ARpi = t*ARpi

        pts3D.append(np.array(Lpi + ARpi - Ainv_b/2).T)

    return np.array(pts3D).reshape(len(pts3D),3)