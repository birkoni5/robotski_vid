import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import numpy as np
import time
import vtk
from vtk.util.numpy_support import vtk_to_numpy
import os.path as path
from os import listdir
from os.path import isfile, join

iterations = [10,50,100,500,1000]
landmarks = [10,50,100,500,1000]

optimal_i_idx = [1,1,2,2,1] #len == num of files
optimal_l_idx = [1,1,3,0,2]

l_mash, i_mesh= np.meshgrid(landmarks, iterations)
z_labels = ['time[s]', 'log10(mse)']

actors = []
mses = []
times = []

models_path = path.abspath("3D_models") + '\\'
files = [f for f in listdir(models_path) if isfile(join(models_path, f))]

reader = vtk.vtkPLYReader()
reader.SetFileName(models_path + files[0])
reader.Update()
polydata = reader.GetOutput()

ref_pts = vtk_to_numpy(polydata.GetPoints().GetData())

mapper = vtk.vtkPolyDataMapper()
mapper.SetInputConnection(reader.GetOutputPort())
ref_actor = vtk.vtkActor()
ref_actor.SetMapper(mapper)

target = polydata

data_prepared = 0

def MSE(Y, YH):
    return np.square(Y - YH).mean()

def CalculateAllICPData():
    """
    actors[file_idx] -> actors for all combinations of the number of landmarks and iterations
    actors[file_idx][0] -> untransformed model
    actors[file_idx][1] -> first combination
                ...
    actors[file_idx][25] -> last combination

    mses[file_idx] -> matrix of mean squared errors for all combinations

    times[file_idx] -> matrix of execution times for all combinations
    """
    for i in range(1,len(files)):
        reader = vtk.vtkPLYReader()
        reader.SetFileName(models_path + files[i])
        reader.Update()
        polydata = reader.GetOutput()
    
        # untransformed_pts = vtk_to_numpy(polydata.GetPoints().GetData())
    
        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputConnection(reader.GetOutputPort())
    
        actor = vtk.vtkActor()
        actor.SetMapper(mapper)
        actor.GetProperty().SetColor(1.0, 1.0, 0.0)
    
        source = polydata
        f_actors = [actor]
        f_mses = [[0 for i in range(len(iterations))] for j in range(len(landmarks))] #[landmarks,iterations]
        f_times = [[0 for i in range(len(iterations))] for j in range(len(landmarks))]

        
        print("--- Calculating data for:",files[i],"---")
        for j in range(len(landmarks)):
            for k in range(len(iterations)):
                start = time.time()
    
                icp = vtk.vtkIterativeClosestPointTransform()
                icp.SetSource(source)
                icp.SetTarget(target)
                icp.GetLandmarkTransform().SetModeToRigidBody()
                icp.SetMaximumNumberOfIterations(iterations[k])
                icp.SetMaximumNumberOfLandmarks(landmarks[j])
                icp.Update()
    
                icpTransformFilter = vtk.vtkTransformPolyDataFilter()
                icpTransformFilter.SetInputData(source)
                icpTransformFilter.SetTransform(icp)
                icpTransformFilter.Update()
    
                end = time.time()
    
                polydata = icpTransformFilter.GetOutput()
                transformed_pts = vtk_to_numpy(polydata.GetPoints().GetData())
    
                mse = MSE(ref_pts, transformed_pts)
    
                mapper = vtk.vtkPolyDataMapper()
                mapper.SetInputConnection(icpTransformFilter.GetOutputPort())
                transformed_actor = vtk.vtkActor()
                transformed_actor.SetMapper(mapper)
                transformed_actor.GetProperty().SetColor(0.0, 1.0, 0.0)
    
    
                f_actors.append(transformed_actor)
                f_times[j][k] = end - start
                f_mses[j][k] = mse
        
        actors.append(f_actors)
        global times, mses
        times.append(f_times)
        mses.append(f_mses)
        
    times = np.array(times)
    mses = np.array(mses)

def showWindows(model_idx, l_idx, i_idx):
    """
    model_idx -> number between 0 and 4
    l_idx -> number between 0 and 4
    i_idx -> number between 0 and 4
    """
    window1 = vtk.vtkRenderWindow()
    window1.SetSize(500, 500)
    interactor1 = vtk.vtkRenderWindowInteractor()
    interactor1.SetRenderWindow(window1)
    
    text_actor = vtk.vtkTextActor()
    text_actor.SetInput("untransformed model")
    text_actor.SetPosition(10, 40)
    text_actor.GetTextProperty().SetFontSize(24)
    
    renderer = vtk.vtkRenderer()
    window1.AddRenderer(renderer)
    renderer.AddActor(ref_actor)
    renderer.AddActor(actors[model_idx][0])
    renderer.AddActor2D(text_actor)
    renderer.SetBackground(0.1, 0.1, 0.4)
    window1.Render()

    window2 = vtk.vtkRenderWindow()
    window2.SetSize(500, 500)
    interactor2 = vtk.vtkRenderWindowInteractor()
    interactor2.SetRenderWindow(window2)
    
    text_actor = vtk.vtkTextActor()
    text_actor.SetInput("iterations: " + str(iterations[i_idx]) + ", landmarks: " + str(landmarks[l_idx]))
    text_actor.SetPosition(10, 40)
    text_actor.GetTextProperty().SetFontSize(24)
    
    renderer = vtk.vtkRenderer()
    window2.AddRenderer(renderer)
    renderer.AddActor(ref_actor)
    renderer.AddActor(actors[model_idx][l_idx*len(iterations)+i_idx+1]) ## +1 because the first one is an untransformed model
    renderer.AddActor2D(text_actor)
    renderer.SetBackground(0.1, 0.1, 0.4)

    window2.Render()
    interactor1.Start()
    interactor2.Start()


def plotICPData(model_idx, time_mse): 
    """
    model_idx -> number between 0 and 4
    time_mse -> 0 -> plotting time data
             -> 1 -> plotting mse data
    """
    fig = plt.figure()
    if time_mse:
        z = np.log10(mses[model_idx])
    else:
        z = times[model_idx]
    ax = plt.axes(projection='3d')
    ax.plot_surface(l_mash, i_mesh, z, rstride=1, cstride=1,cmap='viridis',edgecolor='none')
    ###TITLE
    ax.set_xlabel('landmarks')
    ax.set_ylabel('iterations')
    ax.set_zlabel(z_labels[time_mse])
    ax.set_zlim3d(np.min(z), np.max(z))
    colorscale = cm.ScalarMappable(cmap=cm.viridis)
    colorscale.set_array(z)
    fig.colorbar(colorscale)
    plt.show()