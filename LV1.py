import cv2
import copy
import os.path as path
from os import listdir
from os.path import isfile, join

img_path = path.abspath("Images") + '\\'
cropped = False
refPt, clone, roi, image = [], [], [], []


def Load_image(img_name):
    img_location = img_path + img_name
    if Image_exists(img_name):
        return cv2.imread(img_location)
    else:
        return 0


def Get_available_images():
    images = [img for img in listdir(img_path) if isfile(join(img_path, img))]
    return images


def Image_exists(img_name):
    return path.isfile(img_path+img_name)


def click_and_crop(event, x, y, flags, param):
    global refPt, cropped, image, roi
    if event == cv2.EVENT_LBUTTONDOWN:
        if cv2.getWindowProperty('ROI', 0) != -1:
            cv2.destroyWindow("ROI")
        refPt = [(x, y)]
    elif event == cv2.EVENT_LBUTTONUP:
        refPt.append((x, y))
        roi = clone[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
        cv2.imshow("ROI", roi)
        image = copy.copy(clone)
        cropped = True
    elif event == cv2.EVENT_MOUSEMOVE:
        image = copy.copy(clone)
        if len(refPt) == 1:
            refPt.append((x, y))
        elif len(refPt) == 2:
            refPt[1] = (x, y)
    return


def cropping(img):
    global cropped, refPt, roi, cropped, clone, image
    image = img
    clone = copy.copy(image)
    cv2.namedWindow("image")
    cv2.setMouseCallback("image", click_and_crop)
    while True:
        if len(refPt) >= 2:  # ako je označen pravokutnik
            cv2.rectangle(image, refPt[0], refPt[1], (0, 255, 0), 1)
        cv2.imshow("image", image)
        key = cv2.waitKey(1) & 0xFF
        # ako je slika cropana i pritisnuto slovo c
        if cropped and key == ord('c'):
            cv2.destroyAllWindows()
            refPt.clear()
            return roi
        elif key == 27:  # ako je pritisnuta tipka ESC vraća se izvorna slika
            cv2.destroyAllWindows()
            return []


def show_image(window_name, image):
    cv2.imshow(window_name, image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return


def show_gray(image):
    cv2.imshow('gray', cv2.cvtColor(image, cv2.COLOR_BGR2GRAY))
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return


def show_canny(image):
    cv2.imshow('canny', cv2.Canny(image, 50, 200, 3))
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return


def show_video(option):
    cap = cv2.VideoCapture(0)
    while(True):
        ret, frame = cap.read()
        if option == 0:
            show = frame
        elif option == 1:
            show = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        else:
            show = cv2.Canny(frame, 50, 200, 3)
        cv2.imshow('frame', show)
        key = cv2.waitKey(1) & 0xFF
        if key == 27:
            break
        elif key == ord('0'):
            option = 0
        elif key == ord('1'):
            option = 1
        elif key == ord('2'):
            option = 2
    cap.release()
    cv2.destroyAllWindows()
    return
