import LV1
import cv2
import numpy as np
import copy

def template_match(image, template, threshold, method):
    img = copy.copy(image)
    w, h = template.shape[1], template.shape[0]
    res = cv2.matchTemplate(img, template, eval(method))
    cv2.normalize(res, res, 0, 1, cv2.NORM_MINMAX, -1)
    if method == 'cv2.TM_SQDIFF' or method == 'cv2.TM_SQDIFF_NORMED':
        res = -1*(res-1)  
    ## moja metoda
    """ loc = np.where(res >= threshold)
    loc = np.asarray(loc[::-1])
    i = 0
    while i < loc.shape[1]:
        j = 0
        while j < i:
            if abs(loc[0][i] - loc[0][j]) < w and abs(loc[1][i] - loc[1][j]) < h:
                if res[loc[1][i]][loc[0][i]] < res[loc[1][j]][loc[0][j]]:
                    loc = np.delete(loc, i, 1)
                else:
                    loc = np.delete(loc, j, 1)
                i -= 1
                break
            else:
                j += 1
        i += 1
    for pt in zip(*loc):
        cv2.rectangle(img, pt, (pt[0] + w, pt[1] + h), (0, 255, 0), 1)"""
    
    ## Non - maximum supression metoda 
    resc = copy.copy(res)
    maxx = np.amax(resc)
    while maxx >= threshold:
        idx = np.where(resc == maxx)
        idx = np.asarray(idx[::-1])
        idx_w = idx[0][0]
        idx_h = idx[1][0]
        cv2.rectangle(img, (idx_w, idx_h), (idx_w + w, idx_h + h), (0, 255, 0), 1)
        window_size = int(w/2) if w < h else int(h/2)
        start_w = idx_w - window_size if idx_w >= window_size else 0
        start_h = idx_h - window_size if idx_h >= window_size else 0
        stop_w = idx_w + window_size if idx_w + window_size <= resc.shape[1] else resc.shape[1]
        stop_h= idx_h + window_size if idx_h + window_size <= resc.shape[0] else resc.shape[0]
        for i in range(start_h,stop_h):
            for j in range(start_w,stop_w):
                resc[i][j]=0
        maxx = np.amax(resc)
    res *= 255
    res = res.astype(np.uint8)
    return res, img
