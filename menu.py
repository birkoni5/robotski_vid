import sys
import os
import LV1
import LV2
import LV3
import LV4
import LV5
import LV6
import LV7
import numpy as np
image = []
main_menu_actions = {}
load_menu_i_actions = {}
load_menu_t_actions = {}
image_menu_actions = {}
video_menu_actions = {}
# =======================
#     MENUS FUNCTIONS
# =======================


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


def main_menu():
    print("Please choose the option:")
    print(" [0] Process an image")
    print(" [1] Process a video from built-in camera")
    print(" [2] Template matching")
    print(" [3] Hough transformation")
    print(" [4] SIFT - Feature matching")
    print(" [5] 3D scene reconstruction from two images")
    print(" [6] RGBD - Dominant plane")
    print(" [7] Iterative closest point")
    print(" [Q] Quit")
    choice = input(" >>  ")
    cls()
    exec_menu(choice, main_menu_actions)
    return


def load_menu_i():
    print("All available images:\n")
    imgs = LV1.Get_available_images()
    for i, img in enumerate(imgs):
        print(' [%d]' % i, img)
    print(" [B] Back")
    choice = input("\n >> ")
    cls()
    if choice.isdigit() and int(choice) < len(imgs):
        global image
        image = LV1.Load_image(imgs[int(choice)])
        print("The image is loaded succesfully.")
        image_menu()
    else:
        exec_menu(choice, load_menu_i_actions)
    return


def image_menu():
    print("Please choose the option:")
    print(" [0] Show the image")
    print(" [1] Show the image in gray")
    print(" [2] Apply Canny filter to the image")
    print(" [3] Crop the image")
    print(" [B] Back")
    choice = input(" >>  ")
    cls()
    global image
    if choice == '0':
        print("\n [] Press any key to go to the image menu")
        LV1.show_image('Image', image)
        cls()
        image_menu()
    elif choice == '1':
        print("\n [] Press any key to go to the image menu")
        LV1.show_gray(image)
        cls()
        image_menu()
    elif choice == '2':
        print("\n [] Press any key to go to the image menu")
        LV1.show_canny(image)
        cls()
        image_menu()
    elif choice == '3':
        print("\n [C] Confirm cropping and return to the image menu")
        print(" [ESC] Return to the image menu")
        image = LV1.cropping(image)
        cls()
        image_menu()
    else:
        exec_menu(choice, image_menu_actions)
    return


def video_menu():
    print("Please choose the option:")
    print(" [0] Show the video")
    print(" [1] Show the video in gray")
    print(" [2] Apply Canny filter to the video")
    print(" [B] Back")
    choice = input(" >>  ")
    cls()
    if choice.isdigit() and int(choice) < 3 and int(choice) >= 0:
        print(" [0] Show the video")
        print(" [1] Show the video in gray")
        print(" [2] Apply Canny filter to the video")
        print(" [ESC] Return to the video menu")
        LV1.show_video(int(choice))
        cls()
        video_menu()
    else:
        exec_menu(choice, video_menu_actions)
    return


def load_menu_t():
    print("All available images:\n")
    imgs = LV1.Get_available_images()
    for i, img in enumerate(imgs):
        print(' [%d]' % i, img)
    print(" [B] Back")
    choice = input("\n >> ")
    cls()
    if choice.isdigit() and int(choice) < len(imgs):
        global image
        image = LV1.Load_image(imgs[int(choice)])
        print("The image is loaded succesfully.")
        template_menu()
    else:
        exec_menu(choice, load_menu_t_actions)
    return


def template_menu():
    print("Select method for template matching:\n")
    methods = ['cv2.TM_CCOEFF', 'cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR',
               'cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED']
    for i, meth in enumerate(methods):
        print(' [%d]' % i, meth)
    print(" [B] Back")
    choice = input("\n >> ")
    cls()
    ch = choice.lower()
    if ch.isdigit() and int(ch) < len(methods):
        picked_meth = methods[int(ch)]
    elif ch == 'b':
        main_menu()
    else:
        print("Invalid selection, please try again.\n")
        template_menu()
    print("Insert the threshold value:")
    try:
        threshold = float(input(" >>  "))
    except ValueError:
        print("Invalid insertion, returned to main menu.\n")
        main_menu()
    cls()
    print("\n [C] Confirm cropping and return to the image menu")
    print(" [ESC] Return to the main menu")
    template = LV1.cropping(image)
    cls()
    if template == []:
        main_menu()
    matching_res, detected_temp = LV2.template_match(
        image, template, threshold, picked_meth)
    while True:
        print(" [0] Show the matching result")
        print(" [1] Show the image with detected templates")
        print(" [B] Return to the main menu")
        choice = input(" >>  ")
        cls()
        if choice.lower() == 'b':
            main_menu()
        elif choice.isdigit() and int(choice) < 2 and int(choice) >= 0:
            if int(choice) == 0:
                LV1.show_image('Matching result', matching_res)
            elif int(choice) == 1:
                LV1.show_image('Detected templates', detected_temp)
        else:
            print("Invalid selection, please try again.\n")
    return


def hough_transform_menu():
    print(" [0] Calibrate the camera")
    print(" [1] Load camera parameters")
    print(" [B] Return to the main menu")
    choice = input(" >>  ")
    cls()
    if choice.lower() == 'b':
        main_menu()
    elif choice.isdigit() and int(choice) < 2 and int(choice) >= 0:
        if int(choice) == 0:
            mtx,dist = LV3.camera_calibration(6, 8, 10)
        elif int(choice) == 1:
            mtx,dist = LV3.load_camera_paremeters()
    else:
        print("Invalid selection, please try again.\n")
        hough_transform_menu()
    while True:
        print(" [0] Compare the original and the undistored view")
        print(" [1] Calculate the distance")
        print(" [B] Return to the main menu")
        choice = input(" >>  ")
        cls()
        if choice.lower() == 'b':
            main_menu()
        elif choice.isdigit() and int(choice) < 2 and int(choice) >= 0:
            if int(choice) == 0:
                print(" [ESC] Return to the previous menu")
                LV3.show_undistored(mtx, dist)
                cls()
            elif int(choice) == 1:
                img = LV3.take_undistored_frame(mtx, dist)
                if img == []:
                    continue
                while True:
                    print("Insert the width of the ROI [cm]:")
                    try:
                        width = float(input(" >>  "))
                        break
                    except ValueError:
                        cls()
                        print("Invalid insertion, try again.\n")
                while True:
                    print("Insert the height of the ROI [cm]:")
                    try:
                        height = float(input(" >>  "))
                        break
                    except ValueError:
                        cls()
                        print("Invalid insertion, try again.\n")
                cls()
                dim = (width,height)
                pts = LV3.select4points(img)
                if len(pts) == 0:
                    cls()
                    continue
                warped = LV3.four_point_transform(img, pts)
                LV1.show_image("Warped", warped)
                res = (dim[0]/np.shape(warped)[1], dim[1]/np.shape(warped)[0])
                line = LV3.find_dominant_line(warped)
                if line == []:
                    cls()
                    print('No lines have been found on the image!')
                    continue
                img = LV3.draw_line(warped, line)
                img = LV3.draw_distance(warped, line, res)
                LV1.show_image("Image", img)
        else:
            print("Invalid selection, please try again.\n")
    return
    
    
def sift_menu():
    while True:
        print(" [0] Show both images")
        print(" [1] Start")
        print(" [B] Return to the main menu")
        choice = input(" >>  ")
        cls()
        if choice.lower() == 'b':
            main_menu()
        elif choice.isdigit() and int(choice) < 2 and int(choice) >= 0:
            if int(choice) == 0:
                LV4.show_images()
            elif int(choice) == 1:
                print("Select an area, and press [C] to confirm cropping.")
                LV4.SIFT_feature_matching()
                cls()
        else:
            print("Invalid selection, please try again.\n")
    return


def reconstruction_menu():
    print(" [0] Show images taken for 3D scene reconstruction")
    print(" [1] Show images with detected SIFT features")
    print(" [2] Show SIFT features after applying epipolar constraint")
    print(" [3] Show the point cloud")
    print(" [B] Return to the main menu")
    choice = input(" >>  ")
    cls()
    if choice.lower() == 'b':
        main_menu()
    elif choice.isdigit() and int(choice) < 4 and int(choice) >= 0:
        print(" [X] Close windows to continue")
        if int(choice) == 0:
            LV5.show_images()
        elif int(choice) == 1:
            LV5.show_SIFT_images()
        elif int(choice) == 2:
            LV5.show_epipolar_SIFT_images()
        elif int(choice) == 3:
            LV5.show_point_cloud()
        cls()
    else:
        print("Invalid selection, please try again.\n")
    reconstruction_menu()


def dominant_menu():
    print("Select a Kinect picture:")
    print(" [0 - 8] Kinect pictures")
    print(" [B] Return to the main menu")
    choice = input(" >>  ")
    cls()
    if choice.lower() == 'b':
        main_menu()
    elif choice.isdigit() and int(choice) < 9 and int(choice) >= 0:
        print("Calculating ...")
        LV6.find_dominant_plane(int(choice))
        cls()
    else:
        print("Invalid selection, please try again.\n")
    dominant_menu()


def icp_menu():
    if LV7.data_prepared == 0:
        print("Preparing data ...")
        LV7.CalculateAllICPData()
        LV7.data_prepared = 1
    print(" Pick a 3D model:")
    print(" [0 - 4] Available models")
    print(" [B] Return to the main menu")
    choice = input(" >>  ")
    cls()
    if choice.lower() == 'b':
        main_menu()
    if choice.isdigit() and int(choice) < 5 and int(choice) >= 0:
        model = int(choice)
        icp_menu2(model)
    else:
        print("Invalid selection, please try again.\n")
    icp_menu()


def icp_menu2(model):
    print(" [0] Plot 3D models")
    print(" [1] Plot 3D time graphs")
    print(" [2] Plot 3D MSE graphs")
    print(" [3] Plot 3D models with optimal parameters")
    print(" [B] Return to the main menu")
    choice = input(" >>  ")
    cls()
    if choice.lower() == 'b':
        main_menu()
    if choice.isdigit() and int(choice) < 4 and int(choice) >= 0:
        plot = int(choice)
        if plot == 1:
            LV7.plotICPData(model, 0)
            print("Returned to ICP menu...")
            icp_menu()
        elif plot == 2:
            LV7.plotICPData(model, 1)
            print("Returned to ICP menu...")
            icp_menu()
        elif plot == 3:
            print("After closing windows restart the application if nothing shows up...")
            LV7.showWindows(model, LV7.optimal_l_idx[model], LV7.optimal_i_idx[model])
            print("Returned to ICP menu...")
            icp_menu()
        else:
            while True:
                print(" Pick a number of landmarks:")
                for i in range(len(LV7.landmarks)):
                    print(" [",i,"]",LV7.landmarks[i])
                print(" [B] Back")
                choice = input(" >>  ")
                cls()
                if choice.isdigit() and int(choice) < len(LV7.landmarks) and int(choice) >= 0:
                    l_idx = int(choice)
                    icp_menu3(model, l_idx)
                if choice.lower() == 'b':
                    icp_menu2(model)
                else:
                    print("Invalid selection, please try again.\n")
    else:
        print("Invalid selection, please try again.\n")
    icp_menu2(model)


def icp_menu3(model, l_idx):
    print(" Pick a number of iterations:")
    for i in range(len(LV7.iterations)):
        print(" [",i,"]",LV7.iterations[i])
    print(" [B] Back")
    choice = input(" >>  ")
    cls()
    if choice.isdigit() and int(choice) < len(LV7.iterations) and int(choice) >= 0:
        i_idx = int(choice)
        print("After closing windows restart the application if nothing shows up...")
        LV7.showWindows(model, l_idx, i_idx)
        print("Returned to ICP menu...")
        icp_menu()
    if choice.lower() == 'b':
        icp_menu2(model)
    else:
        print("Invalid selection, please try again.\n")
    icp_menu3(model,l_idx)


def exec_menu(choice, menu_actions):
    ch = choice.lower()
    try:
        menu_actions[ch]()
    except KeyError:
        print("Invalid selection, please try again.\n")
        menu_actions['menu']()
    return


def exit():
    sys.exit(0)


# =======================
#    MENUS DEFINITIONS
# =======================
main_menu_actions = {
    'menu': main_menu,
    '0': load_menu_i,
    '1': video_menu,
    '2': load_menu_t,
    '3': hough_transform_menu,
    '4': sift_menu,
    '5': reconstruction_menu,
    '6': dominant_menu,
    '7': icp_menu,
    'q': exit
}
load_menu_i_actions = {
    'menu': load_menu_i,
    'b': main_menu
}
load_menu_t_actions = {
    'menu': load_menu_t,
    'b': main_menu
}
image_menu_actions = {
    'menu': image_menu,
    'b': main_menu
}
video_menu_actions = {
    'menu': video_menu,
    'b': main_menu
}
# =======================
#      MAIN PROGRAM
# =======================
if __name__ == "__main__":
    cls()
    # Launch main menu
    main_menu()
