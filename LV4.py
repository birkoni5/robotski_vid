##https://stackoverflow.com/questions/52305578/sift-cv2-xfeatures2d-sift-create-not-working-even-though-have-contrib-instal?fbclid=IwAR3gz3UrzSDMhwV2JAYHCZkcUGqlRMkB8PrKyBDKodZJKXY3LIJtH4LIw1s
import numpy as np
import cv2
import LV1
from matplotlib import pyplot as plt
import sys

img1, img2 = [], []


def resize(image, width):
    # we need to keep in mind aspect ratio so the image does
    # not look skewed or distorted -- therefore, we calculate
    # the ratio of the new image to the old image
    r = width / image.shape[1]
    dim = (width, int(image.shape[0] * r))
    
    # perform the actual resizing of the image and show it
    resized = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
    return resized


def show_images():
    cv2.imshow("Image1", img1)
    cv2.imshow("Image2", img2)
    while cv2.getWindowProperty('Image1', 0) >= 0 or cv2.getWindowProperty('Image2', 0) >= 0:
        cv2.waitKey(10)
    cv2.destroyAllWindows()
    return


def SIFT_feature_matching():
    ROI = LV1.cropping(img1)
    if ROI == []:
        return
    # Initiate SIFT detector
    sift = cv2.xfeatures2d.SIFT_create()

    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(ROI,None)
    kp2, des2 = sift.detectAndCompute(img2,None)

    # BFMatcher with default params
    bf = cv2.BFMatcher()


    # Match descriptors.
    matches = bf.knnMatch(des1,des2, k=2)
    # Apply ratio test
    good_matches = []
    for m,n in matches:
        if m.distance < 0.75*n.distance:
            good_matches.append(m)
    #reference: https://stackoverflow.com/questions/51606215/how-to-draw-bounding-box-on-best-matches
    src_pts = np.float32([ kp1[m.queryIdx].pt for m in good_matches ]).reshape(-1,1,2)
    dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good_matches ]).reshape(-1,1,2)
    M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
    matchesMask = mask.ravel().tolist()
    h,w = ROI.shape[:2]
    pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)

    dst = cv2.perspectiveTransform(pts,M)
    dst += (w, 0)  # adding offset

    img3 = cv2.drawMatches(ROI,kp1,img2,kp2,good_matches, None, flags = 2)

    # Draw bounding box in Red
    img3 = cv2.polylines(img3, [np.int32(dst)], True, (0,255,0),2, cv2.LINE_AA)

    cv2.imshow("result", img3)
    while cv2.getWindowProperty('result', 0) >= 0:
        cv2.waitKey(10)
    cv2.destroyAllWindows()
    return


img1 = LV1.Load_image('SIFT1.jpg') # queryImage
img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
img1 = cv2.cvtColor(img1, cv2.COLOR_GRAY2BGR)# kako bi se pri cropanju vidio zeleni pravokutnik
img1 = resize(img1,800)
img2 = LV1.Load_image('SIFT2.jpg') # trainImage
img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
img2 = resize(img2,800)