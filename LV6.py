import cv2
import Kinect
import pptk
import numpy as np

def get_plane_param(points):
    u = points[:,0]
    v = points[:,1]
    d = points[:,2]
    mat = np.array([[u[0],v[0],1],
                    [u[1],v[1],1],
                    [u[2],v[2],1],])
    try:
        return np.linalg.inv(mat) @ d.T
    except  np.linalg.LinAlgError as err:
        if 'Singular matrix' in str(err):
            print("Singular matrix error, please try again.\n")
        return[]
    
    
def get_inliers(points, plane, thresh):
    dists = np.abs(points[:,2]-(plane[0]*points[:,0]+plane[1]*points[:,1]+plane[2]))
    return np.where(dists<thresh)[0]

Kpics, RGBPics  = Kinect.ReadKinectPics()

def find_dominant_plane(pic):
    points = Kpics[pic][0]
    depth_map = Kpics[pic][1]
    depth_map = cv2.cvtColor(depth_map,cv2.COLOR_GRAY2BGR)

    N = points.shape[0]
    iters = 1000
    inlier_thresh=2.5
    dominant_plane=[]
    dominant_inliers_idx=np.array([])

    for i in range(iters):
        chose_id = np.random.choice(N, 3, replace=False)
        chose_points = points[chose_id, :]
        tmp_plane = get_plane_param(chose_points)
        if tmp_plane == []:
            return
        tmp_inliers = get_inliers(points,tmp_plane,inlier_thresh)
        if tmp_inliers.shape[0] > dominant_inliers_idx.shape[0]:
                dominant_inliers_idx = tmp_inliers
                dominant_plane = tmp_plane

    color = np.ones([points.shape[0],3])*[255,255,255]
    color[dominant_inliers_idx]=[0,0,255]
    dp_idx = np.int32(points[dominant_inliers_idx][:,:2])
    depth_map[dp_idx[:,0],dp_idx[:,1]]=[255,0,0]
    v = pptk.viewer(points)
    v.attributes(color)
    cv2.imshow('RGB',RGBPics[pic])
    cv2.imshow('Depth',depth_map)
    while cv2.getWindowProperty('RGB', 0) >= 0 or cv2.getWindowProperty('Depth', 0) >= 0:
        cv2.waitKey(10)
    v.close()