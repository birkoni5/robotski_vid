import numpy as np
import cv2
import C2Dto3D
import LV1
import LV3
from matplotlib import pyplot as plt
import sys
import pptk

img1, img2 = [], []

mtx,dist = LV3.load_camera_paremeters()
img1 = LV1.Load_image('3DL.jpg')
img2 = LV1.Load_image('3DR.jpg')
img1 = LV3.make_undistored(img1,mtx,dist)
img2 = LV3.make_undistored(img2,mtx,dist)

# img1 = LV1.Load_image('image3.bmp')
# img2 = LV1.Load_image('image4.bmp')
img1 = cv2.cvtColor(img1,cv2.COLOR_BGR2GRAY)
img2 = cv2.cvtColor(img2,cv2.COLOR_BGR2GRAY)

# Initiate SIFT detector
sift = cv2.xfeatures2d.SIFT_create()

# find the keypoints and descriptors with SIFT
kp1, des1 = sift.detectAndCompute(img1,None)
kp2, des2 = sift.detectAndCompute(img2,None)

# FLANN parameters
FLANN_INDEX_KDTREE = 1
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
search_params = dict(checks=600)

flann = cv2.FlannBasedMatcher(index_params,search_params)
matches = flann.knnMatch(des1,des2,k=2)
good = []
pts1 = []
pts2 = []
# ratio test as per Lowe's paper
for i,(m,n) in enumerate(matches):
    if m.distance < 0.75*n.distance:
        good.append(m)
        pts2.append(kp2[m.trainIdx].pt)
        pts1.append(kp1[m.queryIdx].pt)

pts1 = np.int32(pts1)
pts2 = np.int32(pts2)
F, mask = cv2.findFundamentalMat(pts1,pts2,cv2.RANSAC)
# We select only inlier points --- Epipolar constraint
pts1 = pts1[mask.ravel()==1]
pts2 = pts2[mask.ravel()==1]

best = []
for m in good:
    bpts2, bpts1 = np.int32(kp2[m.trainIdx].pt), np.int32(kp1[m.queryIdx].pt)
    for i in range(len(pts2)):
        if np.array_equal(bpts2,pts2[i]) and np.array_equal(bpts1,pts1[i]):
            best.append(m)

img3 = cv2.drawMatches(img1,kp1,img2,kp2,good, None, flags = 2)
img4 = cv2.drawMatches(img1,kp1,img2,kp2,best, None, flags = 2)

mtx = np.array(mtx)
#E = mtx.T @ F @ mtx
E1, mask1 = cv2.findEssentialMat(pts1,pts2,mtx,cv2.RANSAC)
# print("Essential po formuli:")
# print(E)
# print("Essential po funkciji:")
# print(E1)
##pts3D = C2Dto3D.Convert2DPointsTo3DPoints(pts1,pts2,E,mtx)
##np.savetxt('points3d.txt', pts3D, fmt='%.5f')
pts3D1 = C2Dto3D.Convert2DPointsTo3DPoints(pts1,pts2,E1,mtx)
np.savetxt('points3d1.txt', pts3D1, fmt='%.5f')

def show_images():
    cv2.imshow('Left',img1)
    cv2.imshow('Right',img2)
    while cv2.getWindowProperty('Left', 0) >= 0 or cv2.getWindowProperty('Right', 0) >= 0:
        cv2.waitKey(10)
    cv2.destroyAllWindows()

def show_SIFT_images():
    cv2.imshow('Good',img3)
    while cv2.getWindowProperty('Good', 0) >= 0:
        cv2.waitKey(10)
    cv2.destroyAllWindows()

def show_epipolar_SIFT_images():
    cv2.imshow('Best',img4)
    while cv2.getWindowProperty('Best', 0) >= 0:
        cv2.waitKey(10)
    cv2.destroyAllWindows()

def show_point_cloud():
    v = pptk.viewer(pts3D1)
    v.set(point_size=0.01)
    v.attributes(np.ones([pts3D1.shape[0],3])*[255,100,0])


