import numpy as np
import cv2
import os.path as path
from os import listdir
from os.path import isfile, join
pics_path = path.abspath("Images\\KinectPics") + '\\'
def ReadKinectPics():
    """
    FORMAT SPREMANJA PODATAKA:
    KinectPics = [[3D točke, Dubinska slika],  --> od prve slike
                  [3D točke, Dubinska slika],  --> od druge slike
                            ...
    primjeri --> KinectPics[0][0][0][2] -> prva slika, 3D točke, prva točka, dubina točke
                 KinectPics[5][1][100][120] -> šesta slika, Dubinska slika, 101. red, 121. stupac
    """
    KinectPics = []
    RGBPics = []
    files = [f for f in listdir(pics_path) if isfile(join(pics_path, f))]
    for i in range(0,len(files),2):
        D = np.loadtxt(pics_path+files[i])
        RGB = cv2.imread(pics_path+files[i+1])
        RGBPics.append(RGB)
        pts3D = []
        dmin = 2047
        dmax = 0
        for v in range(D.shape[0]):
            for u in range(D.shape[1]):
                if D[v,u]==2047:
                    D[v,u] = -1
                elif D[v,u]<dmin:
                    dmin = D[v,u]
                elif D[v,u]>dmax:
                    dmax = D[v,u]
                if D[v,u] != -1:
                    pts3D.append([v,u,-D[v,u]]) #dodati minus (-) na z komponentu za bolji prikaz oblaka točaka
        D = (D-dmin)*254/(dmax-dmin)+1
        D[D<0] = 0
        D = np.uint8(D)
        KinectPics.append([np.array(pts3D),D])
    return KinectPics, RGBPics